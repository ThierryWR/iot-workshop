
SUMMARY = "The IoT Workshop stuff"
DESCRIPTION = "The IoT Workshop samples and labs"
HOMEPAGE = "http://www.windriver.com"
LICENSE = "GPLv2"
SECTION = "devel"

LIC_FILES_CHKSUM = \
"file://lic.txt;md5=beafc085f00f34f4308b17e79658241c"

PR = "r1"

SRC_URI = "http://www.example.com/downloads/iot-workshop_1.0.2.tgz \
	file://rpm-2016-07-05.zip \
	file://gw_rpm_pgp_public_key \
	file://setup.sh \
	file://rc.local \
	file://configure_nuc"

SRC_URI[md5sum] = "faa969acc8794e0faa2a218c0c50ebd8"
SRC_URI[sha256sum] = "4fb91eb3d0492a5b53d0b38915d8f6ae2d31eac32933d44586928da880339c1b"

FILES_${PN}+= "/root/iot-workshop/*"
FILES_${PN}+= "/root/rpm/*/*"
FILES_${PN}+= "/root/setup.sh"
FILES_${PN}+= "/root/configure_nuc"
FILES_${PN}+= "/etc/gw_rpm_pgp_public_key"
FILES_${PN}+= "/root/rc.local.wifi"

do_install() {
	install -d ${D}/root/iot-workshop
	install -d ${D}/etc
	cp -a ${S}/* ${D}/root/iot-workshop
	cp -a ${S}/../rpm ${D}/root
	cp -a ${S}/../gw_rpm_pgp_public_key ${D}/etc
	cp -a ${S}/../setup.sh ${D}/root
	cp -a ${S}/../configure_nuc ${D}/root
	cp -a ${S}/../rc.local ${D}/root/rc.local.wifi
}

INSANE_SKIP_${PN} = "dev-so already-stripped"
