#!/bin/bash

#
# updating mraa and upm packages to support connection with Arduino/Genuino 101
#

MACHINE=`uname -m`

BEAGLEBONE_MACH="armv7l"
ATOM64_MACH="x86_64"

if [ "$MACHINE" == "$ATOM64_MACH" ]; then
  # This is probably a 64 bit Atom gateway
  echo "64 bit gateway"
  rpm --import /etc/gw_rpm_pgp_public_key
  smart channel --add localRepo type=rpm-dir name=LocalRPMs manual=true priority=1 path=/root/rpm/$MACHINE/
  smart update
  # Disabled the upgrade for npw, as it seems to overwrite some of the changes
  # smart upgrade
  smart install upm
  # allow wra user to access /dev/ttyACM?
  gpasswd -a wra dialout
  echo "Please, reboot the target after running this script"
fi

if [ "$MACHINE" == "$BEAGLEBONE_MACH" ]; then
  # This is ARM device
  echo "ARM armv7l device: installing mraa and upm with FIRMATA support"
  tar xzf /root/rpm/$MACHINE/mraa* -C /
  tar xzf /root/rpm/$MACHINE/upm* -C /
  # allow wra user to access /dev/ttyACM?
  gpasswd -a wra dialout
  echo "Please, reboot the target after running this script"
fi

#
# Adds missing files to be able to run /root/configure_nuc, in order
# to configure wireless interface. And also replace the rc.local
# file to initialize the wireless interface
#
# This is done only for non IDP OS
#

UNAME=`uname -a`
IDP="IDP"

if [ "$UNAME" == "${UNAME%$IDP*}" ]; then
  # This is NOT IDP
  if [ "$MACHINE" == "$ATOM64_MACH" ]; then
    echo "x86_64 non-IDP platform: updating wifi support"
    mkdir /etc/wpa_supplicant
    ln -s /etc/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
    cp /etc/rc.local /etc/rc.local.orig
    cp /root/rc.local.wifi /etc/rc.local
    mkdir /var/lib/nuc_config_tools
  fi
fi

#
# switch default interface in /opt/hdc/etc/agent.cfg to use wlan0 instead 
# of eth0 if using wireless to connect to HDC
#

# if [ "$UNAME" == "${UNAME%$IDP*}" ]; then
#   sed -i -r 's/eth0/wlan0/' /opt/hdc/etc/agent.cfg
# fi

#
# Create link to appropriate startup.bin to kick start the iot agent
#

# ln -s /opt/intel/ubroker/bin/startup.bin.wks \
#	/opt/intel/ubroker/bin/startup.bin
